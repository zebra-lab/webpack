# README #

Данный проект содержит пример конфигурации Webpack с комментариями.

### Для кого преднозначен это проект? ###


Данный проект подготовлен на основе [скринкаста по Webpack](https://www.youtube.com/playlist?list=PLDyvV36pndZHfBThhg4Z0822EEG9VGenn)

Скринкаст написан для webpack v1 который в данный момент устарел.
Все конфиги были переписаны по актуальной инструкции под webpack 3.6.0

 * [Актуальная документация](https://webpack.js.org/configuration/)
 * [Устаревшая документация для webpack v1](https://webpack.github.io/docs/)

### Как развернуть проект? ###

`npm install`
