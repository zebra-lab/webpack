import join from 'lodash/join';
import welcome from 'welcome';

function component() {
  let element = document.createElement('div');
  
  // Lodash, now imported by this script
  element.innerHTML = join(['He1low', 'webpack'], ' ');
  console.log(welcome.greet(welcome.name));
  
  // NODE_ENV доступен в коде проэкта.
  console.log(process.env.NODE_ENV);
  return element;
}

document.body.appendChild(component());

