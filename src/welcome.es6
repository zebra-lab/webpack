let name = 'Andrew!';
let greet = (name) => `Hello ${name}`;
export default {name, greet};
