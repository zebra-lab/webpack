module.exports = {
  output: {
    filename: '[name].es5.bundle.js' // string
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx|es6)$/,
        exclude: /(node_modules|bower_components)/,
        use: [
          {
            // https://webpack.js.org/loaders/babel-loader/
            // https://www.youtube.com/watch?v=vkK-h1bgUIE&list
            loader: 'babel-loader',
            // https://babeljs.io/docs/usage/api/#options
            options: {
              presets: [
                ['env', {
                  'targets': {
                    'ie': 6
                  }
                }]
              ],
              minified: true
            }
          }
        ]
      }
    ]
  }
};
