// Инициализация встроенного модуля пути node.js,
// чтобы избежать проблем на разных платформах.
const path = require('path');
// Подключение встроенных плагинов webpack
// (используется для NODE_ENV DefinePlugin).
const webpack = require('webpack');

const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const base_dir = path.resolve(__dirname, './../../');

/**
 * Видео по базовой документации.
 * @link https://www.youtube.com/watch?v=DJSZKf9GkUs
 *
 * @param env
 * @link https://webpack.js.org/guides/environment-variables/
 *  - env.NODE_ENV [local|stage|prod]
 *    показывает environment на котором выполняется скрипт.
 *    Удобно когда settings разные для разных environments
 *  - env.production [true|false]
 *    @link https://webpack.js.org/guides/production/
 *    Не индикатор environment, скорее показывает оптимизировать или нет сборку.
 *  Длинное обсуждение о разнице env.NODE_ENV vs env.production.
 *  @link https://github.com/webpack/webpack/issues/2537#issuecomment-327310858
 *
 * @returns config
 */
module.exports = {
  context: path.resolve(base_dir, 'src'),
  // Точка входа компилятора.
  // string | object | array
  // https://youtu.be/JYP0e7uwgG0
  entry: {
    index: './index.js',
    contact: './contact.js',
    common: './common.js'
  },
  
  // Настройки компиляции
  output: {
    // папка для всех скомпилированных файлов
    path: path.resolve(base_dir, 'dist'), // string
    
    // the filename template for entry chunks
    filename: '[name].bundle.js', // string
    
    // URL-адрес скомилированной папки по отношению к HTML-странице
    publicPath: '/', // string
    
    // Имя экспортируемой библиотеки
    // Дает внешний доступ к функциям приложения.
    // @link https://www.youtube.com/watch?v=AUS-QEp4NUo
    library: 'MyLibrary', // string,
    
    // тип экспортируемой библиотеки
    libraryTarget: 'umd' // universal module definition
  },
  
  plugins: [
    // Палгин определяет глобальные переменные,
    // которые доступны в коде проэкта.
    // https://webpack.js.org/plugins/define-plugin/
    new webpack.DefinePlugin({
      'LANG': JSON.stringify('und')
    }),
    
    // Плагин чистит build папку(и) перед компиляцией.
    // https://github.com/johnagan/clean-webpack-plugin
    new CleanWebpackPlugin(['dist']),
    
    // Плагин для создания динамического HTML.
    // https://github.com/jantimon/html-webpack-plugin
    new HtmlWebpackPlugin({
      title: 'Zebra Lab Webpack'
    }),
    // Плагин предотвращает компиляцию, если сборка содержит ошибки
    // https://youtu.be/R0OxO-iJmws
    // https://webpack.js.org/plugins/no-emit-on-errors-plugin/
    new webpack.NoEmitOnErrorsPlugin(),
    // Плагин выделяет из зборки файл, который является общим для всего проекта,
    // на других страницах можно грузить тогда только тот JS,
    // который нужно для конкретной страницы.
    // https://youtu.be/pSKd2zkEAZM
    // https://webpack.js.org/plugins/commons-chunk-plugin/
    new webpack.optimize.CommonsChunkPlugin({
      name: 'commons',
      filename: 'commons.js'
    })
  ],
  
  // Секция, позволяющая обрабыатвать имена модулей.
  // не использовать с loaders
  resolve: {
    // Папка с модулями.
    modules: [
      'node_modules',
      path.resolve(base_dir, 'src')
    ],
    
    // Расширения, которые будут подставлены в импорте модуля.
    extensions: ['.js', '.jsx', '.es6']
  },
  
  /**
   * @link https://webpack.js.org/configuration/module/
   * @link https://www.youtube.com/watch?v=J-Q7PcfyiGU
   */
  module: {
    rules: [
      {
        test: /\.(js|jsx|es6)$/,
        exclude: /(node_modules|bower_components)/,
        
        // Loader преобразовывает исходный код.
        // https://webpack.js.org/concepts/loaders/
        use: []
      }
    ]
  }
};
