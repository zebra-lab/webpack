const webpack = require('webpack');
const BabelMinifyPlugin = require("babel-minify-webpack-plugin");

module.exports = {
  plugins: [
    /**
     * Babel minify webpack plugin.
     * @link https://github.com/webpack-contrib/babel-minify-webpack-plugin
     */
    new BabelMinifyPlugin(),
    
    /**
     * @link https://webpack.js.org/guides/environment-variables/
     *  - env.NODE_ENV [local|stage|prod]
     *    показывает environment на котором выполняется скрипт.
     *    Удобно когда settings разные для разных environments
     *  - env.production [true|false]
     *    Не индикатор environment, скорее показывает оптимизировать или нет
     *    сборку. Длинное обсуждение о разнице env.NODE_ENV vs env.production.
     *  @link https://github.com/webpack/webpack/issues/2537#issuecomment-327310858
     */
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('production'),
        'production': true
      }
    })
  ]
};
