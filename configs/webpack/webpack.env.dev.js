const webpack = require('webpack');
const path = require('path');
const base_dir = path.resolve(__dirname, './../../');

module.exports = {
  /**
   * Source maps dev tool.
   * @link https://webpack.js.org/configuration/devtool/
   *
   * Посзволяет отлаживать в консоли код из реальных файлов,
   * а не из агрегированного.
   * Значения конфигурации:
   * - cheap-source-map - если нужно использовать source maps на продакшене.
   * - #inline-source-map - если нужно успользовать source maps при
   *   разработке.
   */
  devtool: 'inline-source-map',
  
  /**
   * @link https://webpack.js.org/configuration/dev-server/
   *
   * @todo https://github.com/gaearon/react-hot-loader
   * https://webpack.js.org/guides/hot-module-replacement/
   * https://youtu.be/amT1h3VLTbg?list=PL3TbIm6dHznVtI86uVtwBLyiLN3A6MD4n&t=2396
   */
  devServer: {
    open: true,
    contentBase: path.join(base_dir, 'dist'),
    compress: true,
    // https://webpack.js.org/configuration/stats/
    stats: 'normal',
    port: 9000
  },
  
  /**
   * Функция отслеживания изменений
   * @link https://www.youtube.com/watch?v=85zatjhaOkE
   */
  watch: true,
  // По умолчанию webpack ждет 300 мс после сохранения в редакторе,
  // можно уменьшить до 100, чтобы сборка была еще быстрее.
  watchOptions: {
    aggregateTimeout: 100
  },
  plugins: [
    // Плагин передает в приложение кастомные глобальные переменные.
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('develop'),
        'production': false
      }
    })
  ]
};
