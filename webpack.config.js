/**
 * @file Данный файл содержит пример конфигурации Webpack с комментариями.
 * Он подготовлен на основе скринкаста по Webpack:
 * @link https://www.youtube.com/playlist?list=PLDyvV36pndZHfBThhg4Z0822EEG9VGenn
 *   Курс написан для webpack v1 который в данный момент устарел. Устаревшая
 *   документация для webpack v1:
 * @link https://webpack.github.io/docs/
 *
 * Актуальная документация:
 * @link https://webpack.js.org/configuration/
 *
 * Все конфиги были переписаны по актуальной инструкции под webpack 3.6.0
 */
const path = require('path');
const merge = require('webpack-merge');
const commonConf = require(path.join(__dirname, 'configs/webpack/webpack.common'));
const prodConf = require(path.join(__dirname, 'configs/webpack/webpack.env.prod'));
const devConf = require(path.join(__dirname, 'configs/webpack/webpack.env.dev'));
const es5Conf = require(path.join(__dirname, 'configs/webpack/webpack.es5'));
const es6Conf = require(path.join(__dirname, 'configs/webpack/webpack.es6'));

module.exports = env => {
  const production = !!(env && env.production);
  
  // Мультикомпиляция.
  // https://youtu.be/enlfe-6VQNM
  let conf;
  let envConf = production ? prodConf : devConf;
  
  conf = [
    merge(commonConf, envConf, es5Conf),
    merge(commonConf, envConf, es6Conf)
  ];
  
  return conf;
};
